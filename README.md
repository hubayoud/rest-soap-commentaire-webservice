# Service de commentaires | API REST & SOAP

## Dépendances
```
$ "@nestjs/common": "^7.0.0"
```
```
$ "@nestjs/core": "^7.0.0"
```
```
$ "@nestjs/platform-express": "^7.0.0"
```
Express est directement intégré dans la lib de NestJs
```
$ "reflect-metadata": "^0.1.13"
```
```
$ "rimraf": "^3.0.2"
```
```
$ "rxjs": "^6.5.4"
```
RXJS, utile pour l'asynchrone, est directement intégré lors de la création du app NestJs.
```
$ "soap": "^0.36.0"
```
Librairie externe qui permet de faire du SOAP en Node Js
```
$ "uuid": "^8.3.2"
```
Création d'identifiant unique pour les commentaires
    

## Installation

```bash
# Installer les dépendances
$ npm install
```

## Lancer les serveurs SOAP et REST

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
  
* L'API REST est alors disponible sur http://localhost:3000/commentaire/commentaireserviceservice  
* L'API SOAP est alors disponible sur http://localhost:8080/commentaire/commentaireserviceservice  
  
# Documentation API REST

## Structure d'un commentaire
En JSON, un commentaire est de la forme :  
```
{
	id: number | string; 	// id du commentaire (optionnel)
	dateCreation: string; 	// date de création du commentaire (optionnel)
	auteur: string;	// auteur du commentaire (optionnel)
	titre: string;	// titre du commentaire (optionnel)
}
```  
Tous les champs sont facultatifs.

### Récupérer tous les commentaires
Route `/commentaire/commentaireserviceservice`  
Action `GET`  
Fonction `readAll`  
Paramètres `aucun`  
**Description** : Récupérer tous les commentaires disponibles.  

### Récupérer un commentaire précis
Route `/commentaire/commentaireserviceservice/:id`  
Action `GET`  
Fonction `read`  
Paramètres `id`  
**Description** : Récupérer tous les commentaires disponibles à partir de l'`id` donné. Si l'`id` fourni ne correspond à aucun commentaire, une erreur 404 est retournée.  

### Créer un commentaire
Route `/commentaire/commentaireserviceservice`  
Action `POST`  
Fonction `create`  
Paramètres `aucun`  
**Description** : Créer un commentaire. Aucun paramètre n'est obligatoire mais si un commentaire n'a pas d'id, un UUID lui sera attribué avant d'être enregistré.  
**Exemple** :  
```
{
	"dateCreation": "20210102T15:23",
	"auteur": "Marie Elnar",
}
```  
créera le commentaire suivant :  
```
{
	"dateCreation": "20210102T15:23",
	"auteur": "Marie Elnar",
	"id": "83b32feb-6f89-4c49-ad90-9864a204d20a"
}
```  

### Modifier un commentaire  
Route `/commentaire/commentaireserviceservice/:id`  
Action `UPDATE`  
Fonction `update`  
Paramètres `id`  
**Description** : Modifier les informations d'un commentaire déjà créé. Si l'`id` fourni ne correspond à aucun commentaire, une erreur 404 est retournée.  
  

### Supprimer un commentaire  
Route `/commentaire/commentaireserviceservice/:id`  
Action `DELETE`  
Fonction `delete`  
Paramètres `id`  
**Description** : Supprimer un commentaire à partir de son `id`. Si l'`id` fourni ne correspond à aucun commentaire, une erreur 404 est retournée.  


---


# Documentation API SOAP
Le fichier WSDL associé se trouve dans à la racine du projet.
Utiliser le logiciel `SOAP UI` pour obtenir toues les operations disponibles à partir du fichier WSDL.

## Exemple avec la création d'un commentaire :
```
<soapenv:Header/>
<soapenv:Body>
	<com:create>
			<!--Optional:-->
			<arg0>
				<!--Optional:-->
				<auteur>Georges Orwell</auteur>
				<!--Optional:-->
				<dateCreation>12.23.2020 15:23:56</dateCreation>
				<!--Optional:-->
				<id></id>
				<!--Optional:-->
				<titre>Ceci est mon commentaire</titre>
			</arg0>
	</com:create>
</soapenv:Body>
```

La réponse sera alors : 
```
<soap:Body>
	<tns:createResponse>
			<auteur>Georges Orwell</auteur>
			<dateCreation>Wed Dec 23 2020 15:23:56 GMT+0100 (GMT+01:00)</dateCreation>
			<id>3c4679e8-fed3-45f4-98ce-00583a0e5847</id>
			<titre>Ceci est mon commentaire</titre>
	</tns:createResponse>
</soap:Body>
```

* La date de création en entrée est de type `Date` et est ensuite transformée en `string`.
Si une date invalide est passée en entrée, le commentaire sera créé mais contiendra la date actuelle au moment de la création
```
<dateCreation>Wed Dec 23 2020 15:23:56 GMT+0100 (GMT+01:00)</dateCreation>
```

* L'id a été généré à partir d'un UUID car aucun id n'a été donné en entrée.
```
<id>3c4679e8-fed3-45f4-98ce-00583a0e5847</id>
```