import { Body, Controller, Get, Post, Param, Put, Delete } from '@nestjs/common';
import { Commentaire } from './shared/models/commentaire.model';
import { CommentaireService } from './commentaire/commentaire.service';


@Controller('commentaire/commentaireServiceService')
export class RESTCommentaireController {
	constructor(private readonly commentairesService: CommentaireService) {}

	@Get()
	readAll(): Commentaire[] {
		return this.commentairesService.readAll();
	}

	@Get(':id')
	read(@Param('id') id: string): Commentaire {
		return this.commentairesService.read(+id);
	}

	@Post()
	create(@Body() nouveauCommentaire: Commentaire): Commentaire {
		return this.commentairesService.create(nouveauCommentaire);
	}

	@Put(':id')
	update(@Param('id') id: string, @Body() commentaire: Commentaire): Commentaire {
		return this.commentairesService.update(+id, commentaire);
	}

	@Delete(':id')
	delete(@Param('id') id: string): Commentaire {
		return this.commentairesService.delete(+id);
	}
}
