import * as soap from 'soap';
import * as express from 'express';
import { SOAPService, XML } from './commentaire/soap.utils';
import { NestFactory } from '@nestjs/core';
import { RESTCommentaireModule } from './rest-commentaire.module';

const pathUrl = '/Commentaire/CommentaireServiceService';

async function bootstrap() {
	// For REST API - Bootstrap server
  const RESTapp = await NestFactory.create(RESTCommentaireModule);
  await RESTapp.listen(3000, () => {
		console.log('REST server started on http://localhost:3000')
	});

	// For SOAP API - Bootstrap server
  var SOAPapp = express();
  SOAPapp.listen(8080, () => {
    soap.listen(
			SOAPapp,
			pathUrl,
			SOAPService,
			XML,
			() => {
    		console.log('SOAP server started on http:://localhost:8080');
    	}
		);
  });
}
bootstrap();
