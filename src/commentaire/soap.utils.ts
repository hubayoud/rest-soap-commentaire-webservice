import * as fs from 'fs';
import { CommentaireService } from './commentaire.service';
import * as path from 'path';

// Commentaire service
const commentaireService = new CommentaireService()

// SOAP Service
export const SOAPService = {
  CommentaireServiceService: {
    CommentaireServicePort: {
      readAll: () => {
        return commentaireService.readAll();
      },
      read: (args) => {
        return commentaireService.read(args.arg0);
      },
      create: (args) => {
        return commentaireService.create(args.arg0);
      },
      update: (args) => {
        return commentaireService.update(args.arg0, args.arg1);
      },
      delete: (args) => {
        return commentaireService.delete(args.arg0);
      },
    }
  }
};

// XML
export var XML = fs.readFileSync('CommentaireService.wsdl', 'utf8');
