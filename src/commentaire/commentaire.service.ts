import { Injectable, NotFoundException } from '@nestjs/common';

import { Commentaire } from '../shared/models/commentaire.model';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class CommentaireService {
	static commentaires: Commentaire[] = [
		{
			id: 1,
			dateCreation: (new Date()).toString(),
			auteur: "Tom Hanks",
			titre: "Je ne suis pas d'accord avec ce que dit Bruce Wayne"
		},
		{
			id: 2,
			dateCreation: (new Date()).toString(),
			auteur: "Georges Orwell",
			titre: "Je suis d'accord avec ce que dit Bruce Wayne"
		}
	];

	/**
	 * Find all comments saved
	 */
	public readAll(): Commentaire[] {
		return CommentaireService.commentaires;
	}

	/**
	 * Create a new comment
	 * @param newComment 
	 */
	public create(newComment: Commentaire): Commentaire {
		if (!newComment.id) {
			newComment.id = uuidv4();
		}

		newComment.dateCreation = newComment.dateCreation?.toString() === "Invalid Date" ?  (new Date()).toString() : newComment.dateCreation?.toString();
		CommentaireService.commentaires = [...CommentaireService.commentaires, newComment];

		return newComment;
	}

	/**
	 * Find one comment thanks to its id
	 * @param id 
	 */
	public read(id: number): Commentaire {
		const comment: Commentaire = CommentaireService.commentaires.find((comment: Commentaire) => comment.id === id);

		if (!comment) {
			throw new NotFoundException();
		}

		return comment;
	}

	/**
	 * Update a comment thanks to its id
	 * @param id 
	 * @param comment 
	 */
	public update(id: number, comment: Commentaire): Commentaire {
		const index: number = CommentaireService.commentaires.findIndex((comment: Commentaire) => comment.id === id);
		const toUpdateComment: Commentaire = this.read(id);

		if (!toUpdateComment) {
			throw new NotFoundException();
		}

		let date: string;

		if (comment.dateCreation === undefined) {
			date = toUpdateComment.dateCreation;
		} else {
			if (comment.dateCreation.toString() === "Invalid Date") {
				date = (new Date()).toString();
			} else {
				date = comment.dateCreation.toString();
			}
		}
		
		const updatedComment: Commentaire = {
			id: comment.id ? comment.id : toUpdateComment.id,
			dateCreation: date,
			auteur: comment.auteur ? comment.auteur : toUpdateComment.auteur,
			titre: comment.titre ? comment.titre : toUpdateComment.titre,
		};

		CommentaireService.commentaires[index] = updatedComment;

		return updatedComment;
  }
	
	/**
	 * Delete a comment thanks to its id
	 * @param id 
	 */
	public delete(id: number): Commentaire {
		const index: number = CommentaireService.commentaires.findIndex((comment: Commentaire) => comment.id === id);

		if (index === -1) {
			throw new NotFoundException();
		}

		const commentToDelete = CommentaireService.commentaires[index];
		CommentaireService.commentaires = CommentaireService.commentaires.filter((comment: Commentaire) => comment.id !== commentToDelete.id);
	
		return commentToDelete;
	}
}
