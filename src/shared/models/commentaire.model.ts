export interface Commentaire {
	id?: number | string;
	dateCreation?: string;
	auteur?: string;
	titre?: string;
}