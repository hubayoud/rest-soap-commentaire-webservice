import { Module } from '@nestjs/common';
import { RESTCommentaireController } from './rest-commentaire.controller';
import { CommentaireService } from './commentaire/commentaire.service';

@Module({
  imports: [],
  controllers: [RESTCommentaireController],
  providers: [CommentaireService],
})
export class RESTCommentaireModule {}
